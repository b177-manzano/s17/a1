//Create an addStudent() function that will accept a name of the student and add it to the student array.
let student = [];
function addStudent(variable){
	studentNames = student.push(variable); 

	console.log( variable + ' ' + 'was added to the students list.');

}

// Create a countStudents() function that will print the total number of students in the array.
function countStudents(){
	console.log( 'There are a total of ' +  student.length  + ' students.');
}	


//Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

function printStudents(){
	student.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase())).forEach(function(variable){
		console.log(variable);
	});
}

/*
Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
- If one match is found print the message studentName is an enrollee.
- If multiple matches are found print the message studentNames are enrollees.
- If no match is found print the message studentName is not an enrollee.
- The keyword given should not be case sensitive.*/

function findStudent(variable) {
	names = student.filter(function(students){
		return students.toLowerCase().includes(variable.toLowerCase())});

	if(names.length === 0) {
	console.log('No student found with the name. ' + variable + '.');
	}
	else if(names.length === 1) {
		console.log(variable + ' is an enrollee.');
	}
	else {
		console.log(names.join(', ') + ' are enrollees.');
	}
}

